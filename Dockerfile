FROM openjdk:8-jdk-alpine

COPY target/*.jar api.jar
EXPOSE 9090

ENTRYPOINT ["java","-jar","/api.jar"]